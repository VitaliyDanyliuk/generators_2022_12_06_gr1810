# num_squared_lc = [num ** 2 for num in range(1000000)]
# num_squared_gc = (num ** 2 for num in range(1000000))

import cProfile

print(cProfile.run("sum([num ** 2 for num in range(10000000)])"))

print(cProfile.run("sum((num ** 2 for num in range(10000000)))"))